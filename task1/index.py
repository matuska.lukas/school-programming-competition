print("Welcome to first part!")
print("We are going to remove bugs. :)")

try:
    inputFile = open(input("File name: "), "r")
    key = str(inputFile.readline()).replace('\n', '')
    print("Key: {}".format(key))
    stringToDecode = str(inputFile.readline())
    inputFile.close()
    StringForOutput = stringToDecode

    i = 0
    while key in StringForOutput:
        StringForOutput = StringForOutput.replace(key, "")
        print("{} ... {}".format(i, StringForOutput))
        i += 1
    
    print("Before: {}".format(stringToDecode))
    print("After: {}".format(StringForOutput))
    with open("output.txt", "w") as outputFile:
        outputFile.write(StringForOutput)

except:
    print("ERROR")



