
#inputFile = open("turtle.txt", "r")
inputFile = open(input("File name: "), "r")
countOfCommands = int(inputFile.readline())
commands = str(inputFile.readline())
inputFile.close()

turtleStep = 1
coordinates = [0, 0]

for command in commands:
    if command == "S":
        print("Turtle is going to north.".format(turtleStep))
        coordinates[1] = coordinates[1]+turtleStep
    if command == "J":
        print("Turtle is going to south.".format(turtleStep))
        coordinates[1] = coordinates[1]-turtleStep
    if command == "V":
        print("Turtle is going to east.".format(turtleStep))
        coordinates[0] = coordinates[0]+turtleStep
    if command == "Z":
        print("Turtle is going to west.".format(turtleStep))
        coordinates[0] = coordinates[0]-turtleStep

print("Turtle went to {}".format(coordinates))
with open("output.txt", "w") as outputFile:
    outputFile.write("{} {}\n".format(coordinates[0], coordinates[1]))

